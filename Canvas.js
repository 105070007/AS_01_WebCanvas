console.log("Hello!");
console.log("FINAL");


var audio = document.getElementById("bgMusic");
var musicstste = 0;

function playmusic(){
    if(musicstste == 0){
        musicstste = 1;
        var rm = Math.floor(Math.random()*100) % 3;
        if(rm == 0)
            audio.src = "Arms_Dealer.mp3";
        else if(rm == 1)
            audio.src = "How_it_Began.mp3";
        else 
            audio.src = "Biggie.mp3";
            
        audio.play();
        document.getElementById("mus").src = "image/pause.png";
        document.getElementById("mus").style.width = "37px";
        document.getElementById("mus").style.height = "37px";
    }
    else{
        audio.pause();
        audio.currentTime = 0;
        musicstste = 0;
        document.getElementById("mus").src = "image/play.png";
        document.getElementById("mus").style.width = "60px";
        document.getElementById("mus").style.height = "60px";
    }
}

var _canvas = document.getElementById('canvas');
var ctx = _canvas.getContext('2d');  
var pencolor = 0; // 0 = 黑,1 = 紅,2 = 黃,3 = 藍,4 = 綠, 5 = 橘
var rec = 0, circle = 0, tri = 0;
var textx = -100;
var texty = -100;
var rain = 0;

ctx.lineCap='round';
var linewidth=5;
var currentsize = 40, fonttype = 0;
ctx.lineWidth=linewidth;
ctx.font = '40px Arial';

$myslider=$('#myslider');
$myslider.attr({min:1,max:25}).val(linewidth);
$myslider.on('input change',function(){
    linewidth=ctx.lineWidth=parseInt($(this).val());
});

document.getElementById('textboard').addEventListener('keypress', function(event) {
    if (event.keyCode == 13 && mode == 3) {
        ctx.textAlign = 'left';
        ctx.fillText(document.getElementById('textboard').value, textx, texty);
        document.getElementById('textboard').value="";
    }
});

function changefontsize(size) {
    currentsize = size;

    if(fonttype == 0)
        ctx.font = size +'px Arial';   
    else if(fonttype == 1)   
        ctx.font = size +'px serif';
    else 
    ctx.font = size +'px Courier New';
}

function changefontstyle(sty) {
    fonttype = sty;
    if(sty==0)
        ctx.font = currentsize +'px Arial'; 
    else if(sty==1)
        ctx.font = currentsize +'px Serif';
    else
        ctx.font = currentsize +'px Courier New';
}

var x = 0;
var y = 0;
var mode = 0;

function clearcanvas(){
    ctx.clearRect(0, 0, 700, 500);
    document.body.style.cursor = 'default';
}

function draw(){
    mode = 1;
    document.body.style.cursor = 'cell';
}

function eraser(){
    mode = 2;
    document.body.style.cursor = 'grabbing';
}

function text(){
    mode = 3;
    document.body.style.cursor = 'default';
}

function drawrec(){
    mode = 4;
    document.body.style.cursor = 'cell';
}

function drawcircle(){
    mode = 5;
    document.body.style.cursor = 'cell';
}

function drawtri(){
    mode = 6;
    document.body.style.cursor = 'cell';
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };   
};

var cx;
var cy;
function mouseMove(evt) {
    var mousePos = getMousePos(_canvas, evt);

    if(mode == 7 ){
        if(rain == 0){

            rain =1;
            var c1 = Math.floor(Math.random()*10000) % 255;
            var c2 = Math.floor(Math.random()*10000) % 255;
            var c3 = Math.floor(Math.random()*10000) % 255;
            ctx.strokeStyle = "rgb(" + c1 + "," + c2 + "," + c3 + ")";
        }
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.globalCompositeOperation="source-over";
            ctx.stroke();
        
    }
    else if(mode == 1){
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.globalCompositeOperation="source-over";
        
        if(pencolor == 0)
            ctx.strokeStyle = '#000000'
        else if(pencolor == 1)
            ctx.strokeStyle = '#FF0000'
        else if(pencolor == 2)
            ctx.strokeStyle = '#FFFF00'  
        else if(pencolor == 3)
            ctx.strokeStyle = '#0000AA'
        else if(pencolor == 4)
            ctx.strokeStyle = '#55AA00'
        else if(pencolor == 5)
            ctx.strokeStyle = '#FF8800'

        ctx.stroke();
    }else if(mode == 2){
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.globalCompositeOperation="destination-out";
        ctx.stroke();
    }
    else if(mode == 4){

        if(rec == 0){
            cx = mousePos.x;
            cy = mousePos.y;
            rec = 1;
        }

        ctx.globalCompositeOperation="source-over";
        if(pencolor == 0)
            ctx.fillStyle = '#000000'
        else if(pencolor == 1)
            ctx.fillStyle = '#FF0000'
        else if(pencolor == 2)
            ctx.fillStyle = '#FFFF00'  
        else if(pencolor == 3)
            ctx.fillStyle = '#0000AA'
        else if(pencolor == 4)
            ctx.fillStyle = '#55AA00'
        else if(pencolor == 5)
            ctx.fillStyle = '#FF8800'

        if(rec == 1){
            ctx.fillRect(cx,cy,mousePos.x-cx,mousePos.y-cy);
        }
    }
    else if(mode == 5){

        if(circle == 0){
            cx = mousePos.x;
            cy = mousePos.y;
            circle = 1;
        }
        ctx.globalCompositeOperation="source-over";
        if(pencolor == 0)
            ctx.strokeStyle = '#000000'
        else if(pencolor == 1)
            ctx.strokeStyle = '#FF0000'
        else if(pencolor == 2)
            ctx.strokeStyle = '#FFFF00'  
        else if(pencolor == 3)
            ctx.strokeStyle = '#0000AA'
        else if(pencolor == 4)
            ctx.strokeStyle = '#55AA00'
        else if(pencolor == 5)
            ctx.strokeStyle = '#FF8800'

        if(circle == 1){
            ctx.arc(cx,cy,(mousePos.x-cx)/2,0,Math.PI*2);
            ctx.stroke();
        }
    }
    else if(mode == 6){

        if(tri == 0){
            cx = mousePos.x;
            cy = mousePos.y;
            ctx.moveTo(cx,cy);
            tri = 1;
        }

        ctx.globalCompositeOperation="source-over";
        if(pencolor == 0)
            ctx.fillStyle = '#000000'
        else if(pencolor == 1)
            ctx.fillStyle = '#FF0000'
        else if(pencolor == 2)
            ctx.fillStyle = '#FFFF00'  
        else if(pencolor == 3)
            ctx.fillStyle = '#0000AA'
        else if(pencolor == 4)
            ctx.fillStyle = '#55AA00'
        else if(pencolor == 5)
            ctx.fillStyle = '#FF8800'
        
        if(tri == 1){
            ctx.moveTo(cx,cy);
            ctx.lineTo(2*cx-mousePos.x,mousePos.y);
            ctx.lineTo(mousePos.x,mousePos.y);
            ctx.fill();
        }
        
    }
};


canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(_canvas, evt);
    if(mode == 3){
        ctx.globalCompositeOperation="source-over";
        ctx.fillStyle = '#000000'
        if(rec == 0){
            cx = mousePos.x;
            cy = mousePos.y;
            rec = 1;
            textx = cx+15;

            if(currentsize == 20){
                ctx.fillRect(cx,cy,150,30);
                ctx.clearRect(cx+5,cy+5,140,20);
                texty = cy+20;
            }
            else if(currentsize == 40){
                ctx.fillRect(cx,cy,200,60);
                ctx.clearRect(cx+10,cy+10,180,40);
                texty = cy+40;
            }
            else if(currentsize == 60){
                ctx.fillRect(cx,cy,250,80);
                ctx.clearRect(cx+15,cy+15,220,50);
                texty = cy+60;
            }
        }
    }
    evt.preventDefault();  
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);  
    canvas.addEventListener('mousemove', mouseMove, false);
    
});

let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

canvas.addEventListener('mouseup', function() {
    rec = 0;
    circle = 0;
    tri = 0 ;
    
    rain = 0;

    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);

    canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

function goblack(){
    pencolor = 0;
}
function gored(){
    pencolor = 1;
}
function goyellow(){
    pencolor = 2;
}
function goblue(){
    pencolor = 3;
}
function gogreen(){
    pencolor = 4;
}
function goorange(){
    pencolor = 5;
}

function gorainbow(){
    mode = 7;
}

function cUndo() {
    history.go(-1);
}

function cRedo() {
    history.go(1);
}

download_img = function(el) {
    var image = canvas.toDataURL("image/jpg");
    el.href = image;
};

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if( e.state ){
      ctx.putImageData(e.state, 0, 0);
    }
}


var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}











