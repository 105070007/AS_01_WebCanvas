# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

1. Basic components

    (1) Basic control tools:
        按右方工具列中的鉛筆按鈕，即可在 canvas 上畫畫。按橡皮擦扭可在 canvas 上擦拭畫畫的痕跡。
        可點選畫面左上方各個顏色的色塊改變 Brush 的顏色。
        滑動畫面正上方的 slider 可改變 Brush 的大小 （橡皮擦的大小也會隨著 Brush 的大小而改變）。

    (2) Text input:
        按右方工具列中的 "T"，並點選 canvas 上想要呈現文字的位置，即會出現文字方格。
        在網頁左下方的 textbox 中輸入文字，按下 enter 之後，輸入的文字就會在文字方格中顯示。
        另外，在 textbox 的右方，有可以選字體大小跟字型的按鈕（字體大小跟字型都各有三種可以選擇），
        且 canvas 上的文字方格的大小，會依據字體大小的不同而有所變化（字體越大，文字方格越大）。

    (3) Cursor icon: 
        按工具列中的 "鉛筆" 以及 "圖形"（ 圓形 長方形 三角形 ），滑鼠游標會變成十字狀
        按下橡皮擦，滑鼠游標會變成抓取的圖案
        按下工具列中的 "Clear"，滑鼠游標會變回 default 的游標。

    (4) Refresh button
        按下工具列中的 "Clear"，可將整個畫布清空。


2. Advance tools

    (1) Different brush shapes:
        按右方工具列中的 "圓形圖案" 可在 canvas 上畫圓
        按右方工具列中的 "長方形圖案" 可在 canvas 上畫長方形
        按右方工具列中的 "三角形圖案" 可在 canvas 上畫三角形
        畫上述的的三種圖形時，也可以選取 canvas 上方的色塊，來選擇要畫的圖形的顏色。

    (2) Un/Re-do button :
        按右方工具列中的逆時針箭頭，可以 Undo (將 canvas 回復到 前一個的狀態)
        按右方工具列中的順時針箭頭，可以 Redo (將 canvas 回復到 下一個的狀態)
        按瀏覽器的上一頁、下一頁也可以達到上述 Un/Re-do 的效果。
    
    (3) Image tool:
        按工具列下方的“選擇檔案”，選擇一個jpg檔，就可以將圖檔貼到 canvas 上。
    
    (4) Download:
        按工具列中的 “Download” ，即可將 canvas 的圖案下載，檔名為 "myImage.jpg"
    
3. Other tools

    (1) 按工具列中的黑人問號貼圖，即可在 canvas 上畫出顏色為隨機選擇的線條。

    (2) 按工具列中的音樂播放鈕，即可播放音樂
        播放後播放鈕會變成暫停鈕，在按下暫停鈕音樂會暫停
        且每次播放音樂時（包含按暫停再按播放後），播放的音樂會是隨機的（共有三種音樂會隨機被播放）